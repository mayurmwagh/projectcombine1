variable "vpc_cidr" {
    type = string 
    default = "10.0.0.0/16"
}

variable "vpc_tag" {
    default ="ENV-prod"
    #Name = "VPC"
    #ENV  = "PROD"
}

variable "pr-subnet" {
    type = string
    default = "10.0.16.0/20"
}
variable "pr-subnet_tag" {
    type = string
   # Name = "private"
    default = "PROD"
}

variable "pub_subnet" {
    type = string
    default = "10.0.32.0/20"
}
variable "pub_subnet_tag" {
    type = string
    default = "PROD"
}
variable "map_public_ip"{
    type = string
    default = true
}
variable "rout_table_tag"{
    default = "route-tbl-vpc"
}
variable "igw_tags"{
    type = string
    default = "VPC=igw"
}
variable "web-01"{
    type = string 
    default = "ami-0e8286b71b81c3cc1"
}
variable "tags-web-01" {
    type = string 
    default = "prod-web-01"
}
variable "inst-type-web-01"{
    type = string
    default = "t2.micro"
}
