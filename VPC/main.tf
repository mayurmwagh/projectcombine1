resource "aws_vpc" "vpc-terra" {
  cidr_block = var.vpc_cidr
  instance_tenancy = "default"
  tags = {
    name = "vpc-terr"
    ENV = var.vpc_tag
  }
}


resource "aws_subnet" "pr-sub-terr" {
  vpc_id = aws_vpc.vpc-terra.id 
  cidr_block = var.pr-subnet
  tags ={
    Name = "private-subnet"
    ENV = var.pr-subnet_tag
  }
}
resource "aws_subnet" "pub-sub-terra" {
  vpc_id = aws_vpc.vpc-terra.id
  cidr_block = var.pub_subnet
  tags ={
    Name = "public-subnet"
    ENV = var.pub_subnet_tag
    }
    map_public_ip_on_launch = var.map_public_ip
  }
  

  resource "aws_internet_gateway" "igw_terra" {
  vpc_id = aws_vpc.vpc-terra.id

  tags = {
    Name = "main"
  }
  
}
resource "aws_default_route_table" "routetbl" {
  default_route_table_id = aws_vpc.vpc-terra.default_route_table_id
  
   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_terra.id
  }

  tags = {
    Name = "default table"
  }
}


resource "aws_instance" "web-01"{
  ami = var.web-01
  instance_type = var.inst-type-web-01
  subnet_id = aws_subnet.pub-sub-terra.id
  key_name = "frankfurt-key01"                                 
  # vpc_security_group_ids = aws_security_group.security_grp_ids
  tags ={
    Name = "server-01"
    ENV  = var.tags-web-01
  }
}


  