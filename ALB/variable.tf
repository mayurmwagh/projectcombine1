variable "launch_configuration_ami" {
    type = string 
    default = "ami-0e8286b71b81c3cc1"
}
variable "launch_configuration_ins_type" {
    type = string
    default = "t2.micro"
}
variable "launch_configuration_AZ" {
    type = string
    default = "eu-central-1c"
}