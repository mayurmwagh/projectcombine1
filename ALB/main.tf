resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}
resource "aws_launch_configuration" "launch_conf" {
 image_id = var.launch_configuration_ami
 instance_type = var.launch_configuration_ins_type
 
lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "AS-group" {
  name                 = "AS-grp"
  launch_configuration = aws_launch_configuration.launch_conf.id
  availability_zones = ["eu-central-1c", "eu-central-1c"]
  #availability_zones   = var.launch_configuration_AZ
  desired_capacity     = 2 
  min_size             = 1
  max_size             = 2
  #vpc_zone_identifier = aws_default_vpc

  lifecycle {
    create_before_destroy = true
  }
}

######  Alaram and autoscaling increase and decreases ######
