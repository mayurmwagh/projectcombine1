########## launch configuration ############
resource "aws_launch_configuration" "launch_conf" {
 image_id = "ami-0e8286b71b81c3cc1"
 instance_type = "t2.micro"
 
lifecycle {
    create_before_destroy = true
  }

}

################# auto scaling group  ###################
resource "aws_autoscaling_group" "AS-group" {
  name                 = "AS-grp"
  launch_configuration = aws_launch_configuration.launch_conf.id
  availability_zones = ["eu-central-1c", "eu-central-1c"]
  #availability_zones   = var.launch_configuration_AZ
  desired_capacity     = 2 
  min_size             = 1
  max_size             = 2
  #vpc_zone_identifier = aws_default_vpc

  lifecycle {
    create_before_destroy = true
  }
}

#################  auto scaling policy #####################################

resource "aws_autoscaling_policy" "bat" {
  name                   = "foobar3-terraform-test"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.AS-group.name
}

################### increase ########################

resource "aws_cloudwatch_metric_alarm" "increase_alarm" {
  alarm_name                = "increase_alarm"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "80"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = []
    dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.AS-group.name
  }

#   alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.bat.arn]
}

##################### decrease #########################
resource "aws_cloudwatch_metric_alarm" "decrease_alarm" {
  alarm_name                = "decrease_alarm"
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "20"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = []
    dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.AS-group.name
  }
  alarm_actions     = [aws_autoscaling_policy.bat.arn]
}

resource "aws_lb_target_group" "ip-example" {
  name        = "tf-example-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.main.id
}

resource "aws_elb" "load-balancer" {
  name               = "foobar-terraform-elb"
  availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]

  access_logs {
    bucket        = "foo"
    bucket_prefix = "bar"
    interval      = 60
  }

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 8000
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
   # ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }

  instances                   = [aws_instance.foo.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "foobar-terraform-elb"
  }
}


